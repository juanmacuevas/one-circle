# One Circle 

This repository contains a simplistic Android application aiming to demonstrate some of my experience. It is a personal approach to solve the given assessment within a week.

## Overview

The first screen of the app shows a field were we can search for a place using foursquare's API. 
Once the response is available the results are then persisted in the Database.
The UI is observing on the DB table to keep itself updated.
Once tapped on a Venue a second screen displays its details using the same mechanism to fetch and save the data.
The app can show one or two screens simultaneously based on the device width (table/mobile mode).


## Tech Stack:

 - Written in Kotlin
 - Jetpack libraries
 - RecyclerView
 - Viewmodel
 - Livedata
 - Kotlin coroutines
 - Room Dao / sqlite
 - retrofit/gson service
 - Coil images

## Architecture

Inspired by the trendy Androd MVVM Architecture the code is structured in several layers:

 - View (ui: activities & fragments)
 - ViewModel
 - Repository 
 	- Database
 	- API

## Next steps:
The delivered app is minimally functional. Here is a list of tasks to improve the project to the level of production.

 - Show search sugestions (WIP)
 - Improve readability and testability
 - Code coverage
 - Add dependency injection (Dagger,Koin)
 - Rearrange code and package names
 - Deal with Backend errors, quotas, etc.
 - Improve the UI design


## FourSquare API 
https://developer.foursquare.com/docs/api-reference/venues/search/
Returns a list of venues near a search term. We use userless authentication method with a registered cliend ID and Secret

Parameters: 
 - near (required): Search term. If the string is not geocodable, returns a failed_geocode error. Otherwise, searches within the bounds of the geocode and adds a geocode object to the response.
 - radius (meters): 1000 (use with categoryId or query)
 - query: A search term to be applied against venue name. Let's use 'ABN'?
 - categoryId: filter by category. Comma separated values
 	- ATMs category is '52f2ab2ebcbc57f1066b8b56'
 	- Bank category is '4bf58dd8d48988d10a951735'
 - limit: 10
 - url ? 
 - providerId ?
 - linkedId ?

