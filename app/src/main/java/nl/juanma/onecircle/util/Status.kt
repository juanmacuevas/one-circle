package nl.juanma.onecircle.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}