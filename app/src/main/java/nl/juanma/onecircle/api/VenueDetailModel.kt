package nl.juanma.onecircle.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenueDetailModel {
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null

    @SerializedName("notifications")
    @Expose
    var notifications: List<Notification>? =
        null

    @SerializedName("response")
    @Expose
    var response: Response? = null

    inner class Attributes {
        @SerializedName("groups")
        @Expose
        var groups: List<Any>? = null
    }

    inner class BeenHere {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("unconfirmedCount")
        @Expose
        var unconfirmedCount: Int? = null

        @SerializedName("marked")
        @Expose
        var marked: Boolean? = null

        @SerializedName("lastCheckinExpiredAt")
        @Expose
        var lastCheckinExpiredAt: Int? = null
    }

    inner class BestPhoto {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("source")
        @Expose
        var source: Source__? = null

        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

        @SerializedName("width")
        @Expose
        var width: Int? = null

        @SerializedName("height")
        @Expose
        var height: Int? = null

        @SerializedName("visibility")
        @Expose
        var visibility: String? = null
    }

    inner class Category {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("pluralName")
        @Expose
        var pluralName: String? = null

        @SerializedName("shortName")
        @Expose
        var shortName: String? = null

        @SerializedName("icon")
        @Expose
        var icon: Icon? = null

        @SerializedName("primary")
        @Expose
        var primary: Boolean? = null
    }

    inner class Colors {
        @SerializedName("highlightColor")
        @Expose
        var highlightColor: HighlightColor? = null

        @SerializedName("highlightTextColor")
        @Expose
        var highlightTextColor: HighlightTextColor? = null

        @SerializedName("algoVersion")
        @Expose
        var algoVersion: Int? = null
    }

    inner class Contact {
        @SerializedName("phone")
        @Expose
        var phone: String? = null

        @SerializedName("formattedPhone")
        @Expose
        var formattedPhone: String? = null

        @SerializedName("twitter")
        @Expose
        var twitter: String? = null
    }

    inner class DefaultHours {
        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("richStatus")
        @Expose
        var richStatus: RichStatus__? = null

        @SerializedName("isOpen")
        @Expose
        var isOpen: Boolean? = null

        @SerializedName("isLocalHoliday")
        @Expose
        var isLocalHoliday: Boolean? = null

        @SerializedName("dayData")
        @Expose
        var dayData: List<Any>? = null

        @SerializedName("timeframes")
        @Expose
        var timeframes: List<Timeframe__>? = null
    }

    inner class Entity {
        @SerializedName("indices")
        @Expose
        var indices: List<Int>? = null

        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("object")
        @Expose
        var `object`: Object? = null
    }

    inner class Entity_ {
        @SerializedName("indices")
        @Expose
        var indices: List<Int>? = null

        @SerializedName("type")
        @Expose
        var type: String? = null
    }

    inner class Followers {
        @SerializedName("count")
        @Expose
        var count: Int? = null
    }

    inner class Group {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Any>? = null
    }

    inner class Group_ {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item_>? = null
    }

    inner class Group__ {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item___>? = null
    }

    inner class Group___ {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item____>? = null
    }

    inner class Group____ {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item_____>? = null
    }

    inner class HereNow {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("summary")
        @Expose
        var summary: String? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Any>? = null
    }

    inner class HighlightColor {
        @SerializedName("photoId")
        @Expose
        var photoId: String? = null

        @SerializedName("value")
        @Expose
        var value: Int? = null
    }

    inner class HighlightTextColor {
        @SerializedName("photoId")
        @Expose
        var photoId: String? = null

        @SerializedName("value")
        @Expose
        var value: Int? = null
    }

    inner class Hours {
        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("richStatus")
        @Expose
        var richStatus: RichStatus? = null

        @SerializedName("isOpen")
        @Expose
        var isOpen: Boolean? = null

        @SerializedName("isLocalHoliday")
        @Expose
        var isLocalHoliday: Boolean? = null

        @SerializedName("dayData")
        @Expose
        var dayData: List<Any>? = null

        @SerializedName("timeframes")
        @Expose
        var timeframes: List<Timeframe>? = null
    }

    inner class Icon {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Inbox {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Any>? = null
    }

    inner class Item {
        @SerializedName("unreadCount")
        @Expose
        var unreadCount: Int? = null
    }

    inner class Item_ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("source")
        @Expose
        var source: Source? = null

        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

        @SerializedName("width")
        @Expose
        var width: Int? = null

        @SerializedName("height")
        @Expose
        var height: Int? = null

        @SerializedName("user")
        @Expose
        var user: User? = null

        @SerializedName("visibility")
        @Expose
        var visibility: String? = null
    }

    inner class Item__ {
        @SerializedName("summary")
        @Expose
        var summary: String? = null

        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("reasonName")
        @Expose
        var reasonName: String? = null
    }

    inner class Item___ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("text")
        @Expose
        var text: String? = null

        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("canonicalUrl")
        @Expose
        var canonicalUrl: String? = null

        @SerializedName("likes")
        @Expose
        var likes: Likes_? = null

        @SerializedName("like")
        @Expose
        var like: Boolean? = null

        @SerializedName("logView")
        @Expose
        var logView: Boolean? = null

        @SerializedName("agreeCount")
        @Expose
        var agreeCount: Int? = null

        @SerializedName("disagreeCount")
        @Expose
        var disagreeCount: Int? = null

        @SerializedName("todo")
        @Expose
        var todo: Todo? = null

        @SerializedName("user")
        @Expose
        var user: User_? = null

        @SerializedName("authorInteractionType")
        @Expose
        var authorInteractionType: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo___? = null

        @SerializedName("photourl")
        @Expose
        var photourl: String? = null

        @SerializedName("editedAt")
        @Expose
        var editedAt: Int? = null

        @SerializedName("lang")
        @Expose
        var lang: String? = null

        @SerializedName("entities")
        @Expose
        var entities: List<Entity>? =
            null
    }

    inner class Item____ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo_? = null
    }

    inner class Item_____ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("description")
        @Expose
        var description: String? = null

        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("user")
        @Expose
        var user: User__? = null

        @SerializedName("editable")
        @Expose
        var editable: Boolean? = null

        @SerializedName("public")
        @Expose
        var _public: Boolean? = null

        @SerializedName("collaborative")
        @Expose
        var collaborative: Boolean? = null

        @SerializedName("url")
        @Expose
        var url: String? = null

        @SerializedName("canonicalUrl")
        @Expose
        var canonicalUrl: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("updatedAt")
        @Expose
        var updatedAt: Int? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo_____? = null

        @SerializedName("followers")
        @Expose
        var followers: Followers? = null

        @SerializedName("listItems")
        @Expose
        var listItems: ListItems? = null
    }

    inner class Item______ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("tip")
        @Expose
        var tip: Tip? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo________? = null
    }

    inner class LabeledLatLng {
        @SerializedName("label")
        @Expose
        var label: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null
    }

    inner class Likes {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Group>? = null

        @SerializedName("summary")
        @Expose
        var summary: String? = null
    }

    inner class Likes_ {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Group___>? = null

        @SerializedName("summary")
        @Expose
        var summary: String? = null
    }

    inner class Likes__ {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Any>? = null
    }

    inner class ListItems {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item______>? = null
    }

    inner class Listed {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Group____>? = null
    }

    inner class Location {
        @SerializedName("address")
        @Expose
        var address: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

        @SerializedName("labeledLatLngs")
        @Expose
        var labeledLatLngs: List<LabeledLatLng>? =
            null

        @SerializedName("postalCode")
        @Expose
        var postalCode: String? = null

        @SerializedName("cc")
        @Expose
        var cc: String? = null

        @SerializedName("city")
        @Expose
        var city: String? = null

        @SerializedName("state")
        @Expose
        var state: String? = null

        @SerializedName("country")
        @Expose
        var country: String? = null

        @SerializedName("formattedAddress")
        @Expose
        var formattedAddress: List<String>? = null
    }

    inner class Meta {
        @SerializedName("code")
        @Expose
        var code: Int? = null

        @SerializedName("requestId")
        @Expose
        var requestId: String? = null
    }

    inner class Notification {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("item")
        @Expose
        var item: Item? = null
    }

    inner class Object {
        @SerializedName("url")
        @Expose
        var url: String? = null
    }

    inner class Open {
        @SerializedName("renderedTime")
        @Expose
        var renderedTime: String? = null
    }

    inner class Open_ {
        @SerializedName("renderedTime")
        @Expose
        var renderedTime: String? = null
    }

    inner class Open__ {
        @SerializedName("renderedTime")
        @Expose
        var renderedTime: String? = null
    }

    inner class PageUpdates {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Any>? = null
    }

    inner class Photo {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo_ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo__ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo___ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("source")
        @Expose
        var source: Source_? = null

        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

        @SerializedName("width")
        @Expose
        var width: Int? = null

        @SerializedName("height")
        @Expose
        var height: Int? = null

        @SerializedName("visibility")
        @Expose
        var visibility: String? = null
    }

    inner class Photo____ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo_____ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

        @SerializedName("width")
        @Expose
        var width: Int? = null

        @SerializedName("height")
        @Expose
        var height: Int? = null

        @SerializedName("user")
        @Expose
        var user: User___? = null

        @SerializedName("visibility")
        @Expose
        var visibility: String? = null
    }

    inner class Photo______ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo_______ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photo________ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

        @SerializedName("width")
        @Expose
        var width: Int? = null

        @SerializedName("height")
        @Expose
        var height: Int? = null

        @SerializedName("user")
        @Expose
        var user: User_____? = null

        @SerializedName("visibility")
        @Expose
        var visibility: String? = null
    }

    inner class Photo_________ {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null
    }

    inner class Photos {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Group_>? = null
    }

    inner class Phrase {
        @SerializedName("phrase")
        @Expose
        var phrase: String? = null

        @SerializedName("sample")
        @Expose
        var sample: Sample? = null

        @SerializedName("count")
        @Expose
        var count: Int? = null
    }

    inner class Popular {
        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("richStatus")
        @Expose
        var richStatus: RichStatus_? = null

        @SerializedName("isOpen")
        @Expose
        var isOpen: Boolean? = null

        @SerializedName("isLocalHoliday")
        @Expose
        var isLocalHoliday: Boolean? = null

        @SerializedName("timeframes")
        @Expose
        var timeframes: List<Timeframe_>? = null
    }

    inner class Reasons {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Item__>? = null
    }

    inner class Response {
        @SerializedName("venue")
        @Expose
        var venue: Venue? = null
    }

    inner class RichStatus {
        @SerializedName("entities")
        @Expose
        var entities: List<Any>? = null

        @SerializedName("text")
        @Expose
        var text: String? = null
    }

    inner class RichStatus_ {
        @SerializedName("entities")
        @Expose
        var entities: List<Any>? = null

        @SerializedName("text")
        @Expose
        var text: String? = null
    }

    inner class RichStatus__ {
        @SerializedName("entities")
        @Expose
        var entities: List<Any>? = null

        @SerializedName("text")
        @Expose
        var text: String? = null
    }

    inner class Sample {
        @SerializedName("entities")
        @Expose
        var entities: List<Entity_>? = null

        @SerializedName("text")
        @Expose
        var text: String? = null
    }

    inner class Saves
    inner class Source {
        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("url")
        @Expose
        var url: String? = null
    }

    inner class Source_ {
        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("url")
        @Expose
        var url: String? = null
    }

    inner class Source__ {
        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("url")
        @Expose
        var url: String? = null
    }

    inner class Specials {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("items")
        @Expose
        var items: List<Any>? = null
    }

    inner class Stats {
        @SerializedName("tipCount")
        @Expose
        var tipCount: Int? = null

        @SerializedName("usersCount")
        @Expose
        var usersCount: Int? = null

        @SerializedName("checkinsCount")
        @Expose
        var checkinsCount: Int? = null

        @SerializedName("visitsCount")
        @Expose
        var visitsCount: Int? = null
    }

    inner class Timeframe {
        @SerializedName("days")
        @Expose
        var days: String? = null

        @SerializedName("includesToday")
        @Expose
        var includesToday: Boolean? = null

        @SerializedName("open")
        @Expose
        var open: List<Open>? = null

        @SerializedName("segments")
        @Expose
        var segments: List<Any>? = null
    }

    inner class Timeframe_ {
        @SerializedName("days")
        @Expose
        var days: String? = null

        @SerializedName("includesToday")
        @Expose
        var includesToday: Boolean? = null

        @SerializedName("open")
        @Expose
        var open: List<Open_>? = null

        @SerializedName("segments")
        @Expose
        var segments: List<Any>? = null
    }

    inner class Timeframe__ {
        @SerializedName("days")
        @Expose
        var days: String? = null

        @SerializedName("includesToday")
        @Expose
        var includesToday: Boolean? = null

        @SerializedName("open")
        @Expose
        var open: List<Open__>? = null

        @SerializedName("segments")
        @Expose
        var segments: List<Any>? = null
    }

    inner class Tip {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("text")
        @Expose
        var text: String? = null

        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("canonicalUrl")
        @Expose
        var canonicalUrl: String? = null

        @SerializedName("likes")
        @Expose
        var likes: Likes__? = null

        @SerializedName("like")
        @Expose
        var like: Boolean? = null

        @SerializedName("logView")
        @Expose
        var logView: Boolean? = null

        @SerializedName("agreeCount")
        @Expose
        var agreeCount: Int? = null

        @SerializedName("disagreeCount")
        @Expose
        var disagreeCount: Int? = null

        @SerializedName("todo")
        @Expose
        var todo: Todo_? = null

        @SerializedName("saves")
        @Expose
        var saves: Saves? = null

        @SerializedName("user")
        @Expose
        var user: User____? = null
    }

    inner class Tips {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Group__>? = null
    }

    inner class Todo {
        @SerializedName("count")
        @Expose
        var count: Int? = null
    }

    inner class Todo_ {
        @SerializedName("count")
        @Expose
        var count: Int? = null
    }

    inner class User {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo? = null
    }

    inner class User_ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo__? = null
    }

    inner class User__ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo____? = null
    }

    inner class User___ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo______? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null
    }

    inner class User____ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo_______? = null
    }

    inner class User_____ {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("firstName")
        @Expose
        var firstName: String? = null

        @SerializedName("lastName")
        @Expose
        var lastName: String? = null

        @SerializedName("gender")
        @Expose
        var gender: String? = null

        @SerializedName("photo")
        @Expose
        var photo: Photo_________? = null
    }

    inner class Venue {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("contact")
        @Expose
        var contact: Contact? = null

        @SerializedName("location")
        @Expose
        var location: Location? = null

        @SerializedName("canonicalUrl")
        @Expose
        var canonicalUrl: String? = null

        @SerializedName("categories")
        @Expose
        var categories: List<Category>? =
            null

        @SerializedName("verified")
        @Expose
        var verified: Boolean? = null

        @SerializedName("stats")
        @Expose
        var stats: Stats? = null

        @SerializedName("url")
        @Expose
        var url: String? = null

        @SerializedName("likes")
        @Expose
        var likes: Likes? = null

        @SerializedName("like")
        @Expose
        var like: Boolean? = null

        @SerializedName("dislike")
        @Expose
        var dislike: Boolean? = null

        @SerializedName("ok")
        @Expose
        var ok: Boolean? = null

        @SerializedName("venueRatingBlacklisted")
        @Expose
        var venueRatingBlacklisted: Boolean? = null

        @SerializedName("beenHere")
        @Expose
        var beenHere: BeenHere? = null

        @SerializedName("specials")
        @Expose
        var specials: Specials? = null

        @SerializedName("photos")
        @Expose
        var photos: Photos? = null

        @SerializedName("reasons")
        @Expose
        var reasons: Reasons? = null

        @SerializedName("description")
        @Expose
        var description: String? = null

        @SerializedName("hereNow")
        @Expose
        var hereNow: HereNow? = null

        @SerializedName("createdAt")
        @Expose
        var createdAt: Int? = null

        @SerializedName("tips")
        @Expose
        var tips: Tips? = null

        @SerializedName("shortUrl")
        @Expose
        var shortUrl: String? = null

        @SerializedName("timeZone")
        @Expose
        var timeZone: String? = null

        @SerializedName("listed")
        @Expose
        var listed: Listed? = null

        @SerializedName("phrases")
        @Expose
        var phrases: List<Phrase>? = null

        @SerializedName("hours")
        @Expose
        var hours: Hours? = null

        @SerializedName("popular")
        @Expose
        var popular: Popular? = null

        @SerializedName("seasonalHours")
        @Expose
        var seasonalHours: List<Any>? = null

        @SerializedName("defaultHours")
        @Expose
        var defaultHours: DefaultHours? = null

        @SerializedName("pageUpdates")
        @Expose
        var pageUpdates: PageUpdates? = null

        @SerializedName("inbox")
        @Expose
        var inbox: Inbox? = null

        @SerializedName("venueChains")
        @Expose
        var venueChains: List<Any>? = null

        @SerializedName("attributes")
        @Expose
        var attributes: Attributes? = null

        @SerializedName("bestPhoto")
        @Expose
        var bestPhoto: BestPhoto? = null

        @SerializedName("colors")
        @Expose
        var colors: Colors? = null
    }
}