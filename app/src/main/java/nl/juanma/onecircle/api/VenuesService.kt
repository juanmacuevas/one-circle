package nl.juanma.onecircle.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

// https://api.foursquare.com/v2/venues/search?near="Rotterdam%20NL"&radius=1000&query=abn%20amro&categoryID=52f2ab2ebcbc57f1066b8b56&limit=10

interface VenuesService {


    @GET("""venues/search?radius=$RADIUS&limit=$LIMIT&categoryID=$CATEGORY_ID&query=$QUERY&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&v=$VERSIONING""")
    suspend fun searchVenues(
        @Query("near") near: String
    ): Response<VenuesModel>


    @GET("""venues/{id}?client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&v=$VERSIONING""")
    suspend fun fetchVenueDetails(
        @Path("id") id: String
    ): Response<VenueDetailModel>


    companion object {
        // const val NEAR = "Rotterdam NL"
        const val RADIUS = "1000"
        const val LIMIT = "10"
        const val CATEGORY_ID = "52f2ab2ebcbc57f1066b8b56"
        const val QUERY = "" //"abn amro"

        const val CLIENT_ID = "UWPQVP2DWAAJHJ1UHUYPXKGIADUWJKHQ0NTFCQBQAEFPPH5Y"
        const val CLIENT_SECRET = "PGLEY2MOFLL53NB31XL0UEM0BGU2U13FMETUZDIFV1GHYUKS"
        const val VERSIONING = "20200710"


    }

}
