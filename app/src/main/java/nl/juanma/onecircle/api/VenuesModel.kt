package nl.juanma.onecircle.api

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenuesModel() : Parcelable {
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null

    @SerializedName("notifications")
    @Expose
    var notifications: List<Notification>? = null

    @SerializedName("response")
    @Expose
    var response: Response? = null

    constructor(parcel: Parcel) : this()

    inner class BeenHere {
        @SerializedName("lastCheckinExpiredAt")
        @Expose
        var lastCheckinExpiredAt: Int? = null

    }

    inner class Bounds {
        @SerializedName("ne")
        @Expose
        var ne: Ne? = null

        @SerializedName("sw")
        @Expose
        var sw: Sw? = null

    }

    inner class Category {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("pluralName")
        @Expose
        var pluralName: String? = null

        @SerializedName("shortName")
        @Expose
        var shortName: String? = null

        @SerializedName("icon")
        @Expose
        var icon: Icon? = null

        @SerializedName("primary")
        @Expose
        var primary: Boolean? = null

    }

    inner class Center {
        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

    }

    inner class Contact {
        @SerializedName("phone")
        @Expose
        var phone: String? = null

        @SerializedName("formattedPhone")
        @Expose
        var formattedPhone: String? = null

        @SerializedName("twitter")
        @Expose
        var twitter: String? = null

        @SerializedName("facebook")
        @Expose
        var facebook: String? = null

        @SerializedName("facebookUsername")
        @Expose
        var facebookUsername: String? = null

        @SerializedName("facebookName")
        @Expose
        var facebookName: String? = null

    }

    inner class Feature {
        @SerializedName("cc")
        @Expose
        var cc: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("displayName")
        @Expose
        var displayName: String? = null

        @SerializedName("matchedName")
        @Expose
        var matchedName: String? = null

        @SerializedName("highlightedName")
        @Expose
        var highlightedName: String? = null

        @SerializedName("woeType")
        @Expose
        var woeType: Int? = null

        @SerializedName("slug")
        @Expose
        var slug: String? = null

        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("longId")
        @Expose
        var longId: String? = null

        @SerializedName("geometry")
        @Expose
        var geometry: Geometry? = null

    }

    inner class Geocode {
        @SerializedName("what")
        @Expose
        var what: String? = null

        @SerializedName("where")
        @Expose
        var where: String? = null

        @SerializedName("feature")
        @Expose
        var feature: Feature? = null

        @SerializedName("parents")
        @Expose
        var parents: List<Any>? = null

    }

    inner class Geometry {
        @SerializedName("center")
        @Expose
        var center: Center? = null

        @SerializedName("bounds")
        @Expose
        var bounds: Bounds? = null

    }

    inner class HereNow {
        @SerializedName("count")
        @Expose
        var count: Int? = null

        @SerializedName("summary")
        @Expose
        var summary: String? = null

        @SerializedName("groups")
        @Expose
        var groups: List<Any>? = null

    }

    inner class Icon {
        @SerializedName("prefix")
        @Expose
        var prefix: String? = null

        @SerializedName("suffix")
        @Expose
        var suffix: String? = null

    }

    inner class Item {
        @SerializedName("unreadCount")
        @Expose
        var unreadCount: Int? = null

    }

    inner class LabeledLatLng {
        @SerializedName("label")
        @Expose
        var label: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

    }

    inner class Location {
        @SerializedName("address")
        @Expose
        var address: String? = null

        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

        @SerializedName("labeledLatLngs")
        @Expose
        var labeledLatLngs: List<LabeledLatLng>? = null

        @SerializedName("postalCode")
        @Expose
        var postalCode: String? = null

        @SerializedName("cc")
        @Expose
        var cc: String? = null

        @SerializedName("city")
        @Expose
        var city: String? = null

        @SerializedName("state")
        @Expose
        var state: String? = null

        @SerializedName("country")
        @Expose
        var country: String? = null

        @SerializedName("formattedAddress")
        @Expose
        var formattedAddress: List<String>? = null

        @SerializedName("neighborhood")
        @Expose
        var neighborhood: String? = null

    }

    inner class Meta {
        @SerializedName("code")
        @Expose
        var code: Int? = null

        @SerializedName("requestId")
        @Expose
        var requestId: String? = null

    }

    inner class Ne {
        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

    }

    inner class Notification {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("item")
        @Expose
        var item: Item? = null

    }

    inner class Response {
        @SerializedName("venues")
        @Expose
        var venues: List<Venue>? = null

        @SerializedName("geocode")
        @Expose
        var geocode: Geocode? = null

    }

    inner class Stats {
        @SerializedName("tipCount")
        @Expose
        var tipCount: Int? = null

        @SerializedName("usersCount")
        @Expose
        var usersCount: Int? = null

        @SerializedName("checkinsCount")
        @Expose
        var checkinsCount: Int? = null

    }

    inner class Sw {
        @SerializedName("lat")
        @Expose
        var lat: Double? = null

        @SerializedName("lng")
        @Expose
        var lng: Double? = null

    }

    inner class Venue {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("contact")
        @Expose
        var contact: Contact? = null

        @SerializedName("location")
        @Expose
        var location: Location? = null

        @SerializedName("categories")
        @Expose
        var categories: List<Category>? = null

        @SerializedName("verified")
        @Expose
        var verified: Boolean? = null

        @SerializedName("stats")
        @Expose
        var stats: Stats? = null

        @SerializedName("url")
        @Expose
        var url: String? = null

        @SerializedName("allowMenuUrlEdit")
        @Expose
        var allowMenuUrlEdit: Boolean? = null

        @SerializedName("beenHere")
        @Expose
        var beenHere: BeenHere? = null

        @SerializedName("hereNow")
        @Expose
        var hereNow: HereNow? = null

        @SerializedName("referralId")
        @Expose
        var referralId: String? = null

        @SerializedName("venueChains")
        @Expose
        var venueChains: List<Any>? = null

        @SerializedName("hasPerk")
        @Expose
        var hasPerk: Boolean? = null

        @SerializedName("venueRatingBlacklisted")
        @Expose
        var venueRatingBlacklisted: Boolean? = null

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VenuesModel> {
        override fun createFromParcel(parcel: Parcel): VenuesModel {
            return VenuesModel(parcel)
        }

        override fun newArray(size: Int): Array<VenuesModel?> {
            return arrayOfNulls(size)
        }
    }


}