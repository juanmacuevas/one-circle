package nl.juanma.onecircle.database
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface VenueDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(venue: Venue?)

    @Update
    suspend fun update(venue: Venue?)

    @Delete
    suspend fun delete(venue: Venue?)

    @Query("SELECT * FROM venues WHERE search == :search")
    fun getVenuesFromSearch(search: String?):LiveData<List<Venue>>

    @Query("SELECT * FROM venues WHERE fs_id == :fourSquareId")
    fun getVenue(fourSquareId: String?) : LiveData<Venue>

    @Query("SELECT * FROM venues WHERE fs_id == :fourSquareId")
    suspend fun getVenueObject(fourSquareId: String?) : List<Venue>
}