package nl.juanma.onecircle.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "venues")
class Venue {

    var id: Int = 0
    var search: String = ""
    @PrimaryKey()
    var fs_id: String = ""

    var title: String = ""
    var location: String = ""

    var photo: String = ""
    var description: String = ""
    var contact: String = ""
    var address: String = ""
    var rating: String = ""

}