package nl.juanma.onecircle.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SearchDao {
    @Insert
    suspend fun insert(search: Search?)

    @Update
    suspend fun update(search: Search?)

    @Delete
    suspend fun delete(search: Search?)

    @Query("SELECT * FROM search_terms ")//WHERE search LIKE '%:term%'")
    fun getSuggestions(): LiveData<List<Search>>

}

