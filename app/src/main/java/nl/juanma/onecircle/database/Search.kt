package nl.juanma.onecircle.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_terms")
data class Search(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val search: String,
    val population: Int,
    val queries: Int)