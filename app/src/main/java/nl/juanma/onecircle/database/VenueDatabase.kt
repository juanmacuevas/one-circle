package nl.juanma.onecircle.database

import android.content.Context
import android.content.res.Resources
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import nl.juanma.onecircle.R

@Database(entities = arrayOf(Search::class, Venue::class), version = 1, exportSchema = true)
abstract class VenueDatabase : RoomDatabase() {

    abstract fun SearchDao(): SearchDao
    abstract fun VenueDao(): VenueDao

    companion object {
        @Volatile
        private var INSTANCE: VenueDatabase? = null

        fun getDatabase(context: Context, coroutineScope: CoroutineScope): VenueDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    VenueDatabase::class.java,
                    "venue_database"
                )
                    .createFromAsset("databases/venues.db")
//                    .addCallback(SearchCitiesCallback(coroutineScope, context.resources))
                    .build()


                INSTANCE = instance
                return instance
            }
        }



    }

//    private class SearchCitiesCallback(
//        private val scope: CoroutineScope,
//        private val resources: Resources
//    ) : RoomDatabase.Callback() {
//        override fun onCreate(db: SupportSQLiteDatabase) {
//            super.onCreate(db)
//            INSTANCE?.let { database ->
//                scope.launch { prePopulateDatabase(database.VenueDao()) }
//            }
//        }

//        private fun prePopulateDatabase(venueDao: VenueDao){
            //TODO parse CSV
//            resources.openRawResource(R.raw.cities).bufferedReader().use {
////                it.readText()
////            }

//            val jsonString = resources.openRawResource(R.raw.players).bufferedReader().use {
//                it.readText()
//            }
//
//            venueDao.insert()
//        }


//    }
}
