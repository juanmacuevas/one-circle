package nl.juanma.onecircle.ui

import android.os.Bundle
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import nl.juanma.onecircle.R
import nl.juanma.onecircle.database.Venue

/**
 * This activity has different presentations for handset and tablet-size devices.
 *  - On handsets, the activity presents a list of items, which when touched,
 *  lead to a [ItemDetailActivity]
 *  - On tablets, the activity presents the list of items and
 *  item details side-by-side using two vertical panes.
 */
class ItemListActivity : AppCompatActivity() {

    private var twoScreens: Boolean = false
    private lateinit var viewModel: MainViewModel
    private lateinit var venuesAdapter: VenueRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setContentView(R.layout.activity_item_list)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = title

        findViewById<NestedScrollView>(R.id.item_detail_container)?.let {
            twoScreens = true
        }

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        venuesAdapter = VenueRecyclerAdapter(this, twoScreens)
        findViewById<RecyclerView>(R.id.item_list).adapter = venuesAdapter

        setupSearch(findViewById<SearchView>(R.id.search_view))
        setupObservers()
    }

    private fun setupSearch(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.setSearchTerm(query.trim())
                searchView.clearFocus();
                return true
            }
            override fun onQueryTextChange(newText: String): Boolean {
                // viewModel.setSearchTerm(newText)
                return false
            }
        })
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
    }

    private fun setupObservers() {
        viewModel.getVenuesList().observe(this, Observer<List<Venue>> {
            venuesAdapter.changeData(it)
        })
    }

}
//        viewModel.getSuggestions().observe(this, Observer {
//            it?.let { resource ->
//                when (resource.status) {
//                    SUCCESS -> {
//                        for (venue in resource.data!!.response!!.venues!!) {
//
//                            Log.i("ItemListActivity", venue.name.toString())
//
//                        }
//                        Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show()
//
//                    }
//                    ERROR -> {
//                        Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show()
////                        recyclerView.visibility = View.VISIBLE
////                        progressBar.visibility = View.GONE
////                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
//                    }
//                    LOADING -> {
//                        Toast.makeText(this, "LOADING", Toast.LENGTH_LONG).show()
////                        progressBar.visibility = View.VISIBLE
////                        recyclerView.visibility = View.GONE
//                    }
//                }
//            }
//        })



