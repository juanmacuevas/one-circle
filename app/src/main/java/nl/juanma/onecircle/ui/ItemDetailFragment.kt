package nl.juanma.onecircle.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import coil.api.load
import com.google.android.material.appbar.CollapsingToolbarLayout
import nl.juanma.onecircle.R
import nl.juanma.onecircle.database.Venue

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a [ItemListActivity]
 * in two-pane mode (on tablets) or a [ItemDetailActivity]
 * on handsets.
 */
class ItemDetailFragment : Fragment() {


    companion object {
        const val ARG_ITEM_ID = "item_id"
    }

    private lateinit var imageView: ImageView
    private lateinit var rootView: View
    private lateinit var venueID : String
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        venueID = arguments?.getString(ARG_ITEM_ID).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.item_detail, container, false)
        imageView = rootView.findViewById<ImageView>(R.id.image_venue)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getVenueDetails(venueID).observe(viewLifecycleOwner, venueDetailObserver)
        return rootView
    }

    private val venueDetailObserver = Observer<Venue> { venue ->
        activity?.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)?.title = venue.title
            rootView.findViewById<TextView>(R.id.item_detail).text = "${venue.title}\n${venue.location}\n${venue.description}\n${venue.address}"

            imageView.load(venue.photo)



    }


}