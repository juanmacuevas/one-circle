package nl.juanma.onecircle.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import nl.juanma.onecircle.R
import nl.juanma.onecircle.database.Search
import nl.juanma.onecircle.database.Venue

class VenueRecyclerAdapter(
        private val parentActivity: AppCompatActivity,
        private val twoPane: Boolean
) :
    RecyclerView.Adapter<VenueRecyclerAdapter.ViewHolder>() {

    private var values: List<Venue> = emptyList()
    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Venue
            if (twoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putString(ItemDetailFragment.ARG_ITEM_ID, item.fs_id)
                    }
                }
                parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(
                    v.context,
                    ItemDetailActivity::class.java
                ).apply {
                    putExtra(ItemDetailFragment.ARG_ITEM_ID, item.fs_id)
                }
                v.context.startActivity(intent)
            }
        }

    }

    fun changeData(values: List<Venue>){
        this.values = values
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.venue_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.title.text = item.title
        holder.location.text = item.location

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(
            R.id.text_title
        )
        val location: TextView = view.findViewById(
            R.id.text_location
        )


    }
}