package nl.juanma.onecircle.ui


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import nl.juanma.onecircle.VenueRepository
import nl.juanma.onecircle.api.RetrofitBuilder
import nl.juanma.onecircle.database.Venue
import nl.juanma.onecircle.database.VenueDatabase


class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val viewModelJob = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var repository: VenueRepository

    init {
        val db = VenueDatabase.getDatabase(application, scope)
        repository = VenueRepository(
            scope, db.VenueDao(), db.SearchDao(),
            RetrofitBuilder.SERVICE_SERVICE
        )
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    // Search
    private val mutableSearchTerm: MutableLiveData<String> = MutableLiveData()
    private val venuesList = Transformations.switchMap(mutableSearchTerm) { query ->
        repository.getVenues(query)
    }
    fun setSearchTerm(query: String) {
        mutableSearchTerm.value = query
        repository.downloadVenues(query)
    }

    fun getVenuesList(): LiveData<List<Venue>> {
        return venuesList
    }

    // Details

    fun getVenueDetails(id: String): LiveData<Venue> {
        repository.fetchVenueDetails(id)
        return repository.getVenue(id)
    }



}