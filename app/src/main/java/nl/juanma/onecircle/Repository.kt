package nl.juanma.onecircle


import android.util.Log
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import nl.juanma.onecircle.api.VenuesModel
import nl.juanma.onecircle.api.VenuesService
import nl.juanma.onecircle.database.Search
import nl.juanma.onecircle.database.SearchDao
import nl.juanma.onecircle.database.Venue
import nl.juanma.onecircle.database.VenueDao
import java.net.UnknownHostException
import kotlin.coroutines.suspendCoroutine

class VenueRepository(
    private val scope: CoroutineScope,
    private val venueDao: VenueDao,
    private val searchDao: SearchDao,
    private val service: VenuesService
) {

    var allSearches: LiveData<List<Search>> = searchDao.getSuggestions()


    suspend fun insertSearch(search: Search) {
        searchDao.insert(search)
    }


    fun getVenues(searchTerm: String): LiveData<List<Venue>> {
        return venueDao.getVenuesFromSearch(searchTerm)
    }

    fun downloadVenues(query: String) {

        scope.launch {
            try {
                val response = service.searchVenues(query)

                if (response?.isSuccessful) {
                    for (v in (response.body() as VenuesModel)!!.response!!.venues!!) {

                        val venue = Venue()
                        venue.search = query
                        venue.fs_id = v.id!!
                        venue.title = v.name!!
                        venue.location = query
                        v.location?.let {
                            venue.location = it.city ?: it.country ?: ""
                        }
                        venueDao.insert(venue)

                    }

                }
            }catch (error :UnknownHostException){
                
            }

        }


    }

    fun getVenue(id: String): LiveData<Venue> {
        return venueDao.getVenue(id)
    }

    fun fetchVenueDetails(id: String) {
        scope.launch {
            try {
                val response = service?.fetchVenueDetails(id)
                if (response?.isSuccessful) {
                    val venueModel = response.body()!!.response!!.venue
                    val venue = venueDao.getVenueObject(id).first()
//                    val venue = Venue()
                    venueModel?.let {
                        it.location?.let { location ->
                            venue.address = location.address ?: ""
                        }
                        it.bestPhoto?.let {
                            venue.photo = it.prefix +"600x600"+ it.suffix
                        }
                        venue.description = venueModel.description ?: ""
                        venueModel.contact?.let { venue.contact = it.phone ?: it.formattedPhone ?: ""}

//                        venue.rating = venueModel.popular!!
                        venueDao.update(venue)
                        Log.i("TAG", "fetchVenueDetails: "+venue!!.photo)
                    }
                }

            }catch (error :UnknownHostException){
            }
        }
    }

}
