# Assessment
## Part One
Please create an app that shows 10 venues around a given place (e.g. Rotterdam) with a radius of 1000 meters. The App contains 2 screens, list and detail. For the list screen uses the following call to search the venues

For this app make use of Foursquare API:
https://developer.foursquare.com/docs/api/venues/search

Before being able to use this API, you need to create an account:
https://developer.foursquare.com/docs/api/getting-started

The UI should be very simple, a search field and a list with venues.

	/////////////////////////////////////////////// 
	/////////////////////////////////////////////// 
	///                                         /// 
	///               Search                    /// 
	///                                         /// 
	/////////////////////////////////////////////// 
	///                                         /// 
	///                                         /// 
	///    Venue 1                              /// 
	///                                         /// 
	///                                         /// 
	/////////////////////////////////////////////// 
	///                                         /// 
	///                                         /// 
	///    Venue 2                              /// 
	///                                         /// 
	///                                         /// 
	/////////////////////////////////////////////// 
	///                                         /// 
	///                                         /// 
	///    Venue 3                              /// 
	///                                         /// 
	///                                         /// 
	/////////////////////////////////////////////// 
	///////////////////////////////////////////////

Please show at least the title and the location of the venue.

The second screen will appear if a user clicks on a venue. This page contains more detailed information about the venue such as:

- Photo’s
- Title
- Description
- Contact information
- Address
- Rating

For this page use the end-point: 
https://developer.foursquare.com/docs/api/venues/details

## Part Two
Make the app to cache the data in the database to be able to working (partly) offline. The cached data needs to be updated once the connection is available.
